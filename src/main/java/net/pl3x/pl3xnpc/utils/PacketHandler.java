package net.pl3x.pl3xnpc.utils;

import net.pl3x.pl3xnpc.Pl3xNPC;
import net.pl3x.pl3xnpc.listeners.PlayerPacketListener;

import com.bergerkiller.bukkit.common.protocol.PacketType;
import com.bergerkiller.bukkit.common.utils.PacketUtil;

public class PacketHandler {
	public PacketHandler(Pl3xNPC plugin) {
		PacketUtil.addPacketListener(plugin, new PlayerPacketListener(plugin), PacketType.USE_ENTITY);
	}
}
