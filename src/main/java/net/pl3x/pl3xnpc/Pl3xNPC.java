package net.pl3x.pl3xnpc;

import java.util.logging.Level;

import net.pl3x.pl3xnpc.listeners.PlayerListener;
import net.pl3x.pl3xnpc.npc.NPCManager;
import net.pl3x.pl3xnpc.utils.PacketHandler;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xNPC extends JavaPlugin {
	private Integer taskID;
	public Boolean allowVNP = false;
	public Boolean allowWG = false;
	
	public void onEnable() {
		PluginManager pm = Bukkit.getPluginManager();
		loadConfiguration();
		if (!pm.isPluginEnabled("BKCommonLib")) {
			log("&4[ERROR] Required dependency BKCommonLib not found!");
			log("&4[ERROR] Pl3xNPC requires this plugin to be installed and enabled to use.");
			log("&4[ERROR] Please download and install BKCommonLib and restart server!");
			log("&4[ERROR] Pl3xNPC will now disable itself!");
			pm.disablePlugin(this);
			return;
		}
		if (pm.isPluginEnabled("VanishNoPacket")) {
			log("&3Detected VanishNoPacket. Enabling vanish support!");
			allowVNP = true;
		} else
			log("&4VanishNoPacket not found! Disabling vanish support!");
		if (pm.isPluginEnabled("WorldGuard")) {
			log("&3Detected WorldGuard. Enabling protected region support!");
			allowWG = true;
		} else
			log("&4VWorldGuard not found! Disabling protected region support!");
		NPCManager.loadAll(this);
		pm.registerEvents(new PlayerListener(this), this);
		new PacketHandler(this);
		getCommand("npc").setExecutor(new CmdNPC(this));
		restartRenderTask();
		log("&ev" + this.getDescription().getVersion() + " by BillyGalbreath enabled!");
	}
	
	public void onDisable() {
		log("&ePlugin Disabled.");
	}
	
	public void loadConfiguration() {
		if (!getConfig().contains("any-item-on-head")) getConfig().addDefault("any-item-on-head", false);
		if (!getConfig().contains("sound-on-select")) getConfig().addDefault("sound-on-select", true);
		if (!getConfig().contains("animate-on-select")) getConfig().addDefault("animate-on-select", true);
		if (!getConfig().contains("message-format")) getConfig().addDefault("message-format", "<{npc}> {message}");
		if (!getConfig().contains("message-radius")) getConfig().addDefault("message-radius", 5D);
		if (!getConfig().contains("look-at-radius")) getConfig().addDefault("look-at-radius", 10D);
		if (!getConfig().contains("update-interval")) getConfig().addDefault("update-interval", 3);
		if (!getConfig().contains("color-logs")) getConfig().addDefault("color-logs", true);
		if (!getConfig().contains("debug-mode")) getConfig().addDefault("debug-mode", false);
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
	
	public static String colorize(String str) {
		return str.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
	}
	
	public void log (Object obj) {
		if (getConfig().getBoolean("color-logs", true))
			getServer().getConsoleSender().sendMessage(colorize("&3[&d" + getName() + "&3] &r" + obj));
		else
			Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + (colorize((String) obj)).replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
	}
	
	public void restartRenderTask() {
		if (taskID != null)
			getServer().getScheduler().cancelTask(taskID);
		taskID = getServer().getScheduler().scheduleSyncRepeatingTask(this, new RenderTask(this), 0, getConfig().getInt("update-interval", 3));
	}
}
